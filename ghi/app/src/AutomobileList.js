import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);
    const [filterValue, setFilterValue] = useState("");
    const [filteredAutomobiles, setFilteredAutomobiles] = useState([]);
    const [deleteSuccess, setDeleteSuccess] = useState(false);
    const [sortConfig, setSortConfig] = useState({
        key: "",
        direction: "",
    });
    const [editAutomobile, setEditAutomobile] = useState(null);
    const [editDateOfDelivery, setEditDateOfDelivery] = useState(null);
    const [editVin, setEditVin] = useState("");


    const fetchAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchAutomobiles()
    }, []);

    const fetchAutomobilesSearch = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");
        if (response.ok) {
            const data = await response.json();
            const vinList = [];
            data.autos.map((automobile) => vinList.push(automobile.vin));
            setFilteredAutomobiles(vinList);
        }
    };

    useEffect(() => {
        fetchAutomobilesSearch();
    }, []);

    const handleFilterVal = (event) => {
        setFilterValue(event.target.value.toUpperCase());
    };

    const filteredAutos = () => {
        let autosToDisplay = [...automobiles];

        if (filterValue && filterValue.trim() !== "") {
            autosToDisplay = autosToDisplay.filter((automobile) =>
                automobile.vin.toUpperCase().includes(filterValue.trim().toUpperCase())
            );
        }

        if (sortConfig.key) {
            autosToDisplay = autosToDisplay.sort((a, b) => {
                if (a[sortConfig.key] < b[sortConfig.key]) {
                    return sortConfig.direction === "asc" ? -1 : 1;
                }
                if (a[sortConfig.key] > b[sortConfig.key]) {
                    return sortConfig.direction === "asc" ? 1 : -1;
                }
                return 0;
            });
        }

        return autosToDisplay;
    };

    const handleEdit = (automobile) => {
        setEditAutomobile({ ...automobile });
        setEditVin(automobile.vin);
        setEditDateOfDelivery(new Date(automobile.date_of_delivery));
    };

    const handleUpdate = async (vin) => {
        try {
            const response = await fetch(`http://localhost:8100/api/automobiles/${vin}/`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    ...editAutomobile,
                    vin: editVin,
                    date_of_delivery: editDateOfDelivery,
                }),
            });

            if (response.ok) {
                fetchAutomobiles();
            } else {
                console.error("Failed to update automobile");
            }
        } catch (error) {
            console.error("Error updating automobile:", error);
        }

        setEditAutomobile(null);
        setEditDateOfDelivery(null);
        setEditVin("");
    };


    const handleInputChange = (e, key) => {
        if (key === "vin") {
            setEditVin(e.target.value);
        } else if (key === "date_of_delivery") {
            setEditDateOfDelivery(e.target.value);
        } else {
            setEditAutomobile((prev) => ({
                ...prev,
                [key]: e.target.value,
            }));
        }
    };

    const handleDelete = async (vin) => {
        try {
            const response = await fetch(`http://localhost:8100/api/automobiles/${vin}/`, {
                method: "DELETE",
            });
            if (response.ok) {
                fetchAutomobiles();
                setDeleteSuccess(true);
            } else {
                console.error("Failed to delete automobile");
            }
        } catch (error) {
            console.error("Error deleting automobile:", error);
        }
    };

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDeleteSuccess(false);
        }, 3000);

        return () => clearTimeout(timeoutId);
    }, [deleteSuccess]);

    const handleSort = (key) => {
        let direction = "asc";
        if (
            sortConfig &&
            sortConfig.key === key &&
            sortConfig.direction === "asc"
        ) {
            direction = "desc";
        }
        setSortConfig({ key, direction });
    };

    const renderSortArrow = (column) => {
        const isAsc = sortConfig.key === column && sortConfig.direction === "asc";
        const isDesc = sortConfig.key === column && sortConfig.direction === "desc";

        return (
            <span className="sort-arrow">
                <i className={`fas fa-arrow-up ${isAsc ? 'active' : ''}`}></i>
                <i className={`fas fa-arrow-down ${isDesc ? 'active' : ''}`}></i>
            </span>
        );
    };

    return (
        <>
            <div className="container-fluid" style={{ paddingTop: 40 }}>
                <div className="pt-4">
                    <h1 className="pb-2">Our Automobiles</h1>
                    <form>
                        <div className="form mb-3">
                            <input value={filterValue} onChange={handleFilterVal} placeholder="Search by VIN" name="filter-value" id="filter-value" className="form-control" />
                        </div>
                    </form>
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th onClick={() => handleSort("vin")}>VIN {renderSortArrow("vin")}</th>
                                <th onClick={() => handleSort("manufacturer")}>Manufacturer {renderSortArrow("manufacturer")}</th>
                                <th onClick={() => handleSort("model")}>Model {renderSortArrow("model")}</th>
                                <th onClick={() => handleSort("year")}>Year {renderSortArrow("year")}</th>
                                <th onClick={() => handleSort("carrier")}>Carrier {renderSortArrow("carrier")}</th>
                                <th onClick={() => handleSort("location_of_pickup")}>Pickup Location {renderSortArrow("location_of_pickup")}</th>
                                <th onClick={() => handleSort("location_of_delivery")}>Dropoff Location {renderSortArrow("location_of_delivery")}</th>
                                <th onClick={() => handleSort("date_of_delivery")}>Date of Delivery {renderSortArrow("date_of_delivery")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredAutos().map((automobile) => {
                                return (
                                    <tr key={automobile.vin}>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editVin}
                                                    onChange={(e) => handleInputChange(e, "vin")}
                                                />
                                            ) : (
                                                automobile.vin
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.manufacturer}
                                                    onChange={(e) => handleInputChange(e, "manufacturer")}
                                                />
                                            ) : (
                                                automobile.manufacturer
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.model}
                                                    onChange={(e) => handleInputChange(e, "model")}
                                                />
                                            ) : (
                                                automobile.model
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.year}
                                                    onChange={(e) => handleInputChange(e, "year")}
                                                />
                                            ) : (
                                                automobile.year
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.carrier}
                                                    onChange={(e) => handleInputChange(e, "carrier")}
                                                />
                                            ) : (
                                                automobile.carrier
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.location_of_pickup}
                                                    onChange={(e) => handleInputChange(e, "location_of_pickup")}
                                                />
                                            ) : (
                                                automobile.location_of_pickup
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <input
                                                    type="text"
                                                    value={editAutomobile.location_of_delivery}
                                                    onChange={(e) => handleInputChange(e, "location_of_delivery")}
                                                />
                                            ) : (
                                                automobile.location_of_delivery
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <DatePicker
                                                    selected={editDateOfDelivery}
                                                    onChange={(date) => setEditDateOfDelivery(date)}
                                                    showTimeSelect
                                                    timeFormat="HH:mm"
                                                    timeIntervals={15}
                                                    dateFormat="yyyy-MM-dd h:mm aa"
                                                />
                                            ) : (
                                                new Date(automobile.date_of_delivery).toLocaleString('en-US', { month: 'numeric', day: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })
                                            )}
                                        </td>
                                        <td>
                                            {editAutomobile && editAutomobile.vin === automobile.vin ? (
                                                <>
                                                    <i
                                                        className="fas fa-check"
                                                        style={{ cursor: "pointer", marginRight: 10 }}
                                                        onClick={() => handleUpdate(automobile.vin)}
                                                    ></i>
                                                    <i
                                                        className="fas fa-ban"
                                                        style={{ cursor: "pointer" }}
                                                        onClick={() => setEditAutomobile(null)}
                                                    ></i>
                                                </>
                                            ) : (
                                                <>
                                                    <i
                                                        className="fas fa-trash"
                                                        style={{ cursor: "pointer", marginRight: 10 }}
                                                        onClick={() => handleDelete(automobile.vin)}
                                                    ></i>
                                                    <i
                                                        className="fas fa-pen-to-square"
                                                        style={{ cursor: "pointer" }}
                                                        onClick={() => handleEdit(automobile)}
                                                    ></i>
                                                </>
                                            )}
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                        {deleteSuccess && (
                            <div className="alert alert-danger mt-3 bg-danger text-white" role="alert">
                                Automobile successfully deleted.
                            </div>
                        )}
                    </table>
                </div>
            </div>
        </>
    );
}

export default AutomobileList;
