import React, { useState, useEffect } from "react";


function CarrierList() {
    const [carriers, setCarriers] = useState([]);
    const [filteredCarrier, setFilteredCarrier] = useState([]);
    const [filterValue, setFilterValue] = useState("");
    const [deleteSuccess, setDeleteSuccess] = useState(false);
    const [sortConfig, setSortConfig] = useState({
        key: "",
        direction: "",
    });
    const [editCarrier, setEditCarrier] = useState(null);

    const fetchCarriers = async () => {
        const response = await fetch('http://localhost:8100/api/carriers/');
        if (response.ok) {
            const data = await response.json();
            setCarriers(data.carriers);
        }
    }

    useEffect(() => {
        fetchCarriers()
    }, []);

    const fetchCarrierSearch = async () => {
        const response = await fetch("http://localhost:8100/api/carriers/");
        if (response.ok) {
            const data = await response.json();
            const carrierList = [];
            data.carriers.map((carrier) => carrierList.push(carrier.dot));
            setFilteredCarrier(carrierList);
        }
    };

    useEffect(() => {
        fetchCarrierSearch();
    }, []);

    const handleFilterVal = (event) => {
        setFilterValue(event.target.value.toUpperCase());
    };

    const filteredCarriers = () => {
        if (filterValue === " ") {
            return carriers;
        } else {
            return carriers.filter((carrier) =>
                carrier.name.toUpperCase().includes(filterValue)
            );
        }
    };

    const handleDelete = async (dot) => {
        try {
            const response = await fetch(`http://localhost:8100/api/carrier/${dot}/`, {
                method: "DELETE",
            });
            if (response.ok) {
                fetchCarriers();
                setDeleteSuccess(true);
            } else {
                console.error("Failed to delete carrier");
            }
        } catch (error) {
            console.error("Error deleting carrier:", error);
        }
    };

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setDeleteSuccess(false);
        }, 3000);

        return () => clearTimeout(timeoutId);
    }, [deleteSuccess]);



    const handleEdit = (carrier) => {
        setEditCarrier({ ...carrier });
    };

    const handleUpdate = async (dot) => {
        try {

            const response = await fetch(`http://localhost:8100/api/carrier/${dot}/`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(editCarrier),
            });
            console.log(response)
            if (response.ok) {
                fetchCarriers();
            } else {
                console.error("Failed to update carrier");
            }
        } catch (error) {
            console.error("Error updating carrier:", error);
        }

        setEditCarrier(null);
    };

    const handleInputChange = (e, key) => {
        setEditCarrier((prev) => ({
            ...prev,
            [key]: e.target.value,
        }));
    };

    const handleSort = (key) => {
        let direction = "asc";
        if (
            sortConfig &&
            sortConfig.key === key &&
            sortConfig.direction === "asc"
        ) {
            direction = "desc";
        }

        const sortedCarriers = [...carriers].sort((a, b) => {
            if (a[key] < b[key]) {
                return direction === "asc" ? -1 : 1;
            }
            if (a[key] > b[key]) {
                return direction === "asc" ? 1 : -1;
            }
            return 0;
        });

        setCarriers(sortedCarriers);
        setSortConfig({ key, direction });
    };

    const renderSortArrow = (column) => {
        const isAsc = sortConfig.key === column && sortConfig.direction === "asc";
        const isDesc = sortConfig.key === column && sortConfig.direction === "desc";

        return (
            <span className="sort-arrow">
                <i className={`fas fa-arrow-up ${isAsc ? 'active' : ''}`}></i>
                <i className={`fas fa-arrow-down ${isDesc ? 'active' : ''}`}></i>
            </span>
        );
    };

    return (
        <>
            <div className="container" style={{ paddingTop: 40 }}>
                <div className="pt-4">
                    <h1 className="pb-2">List of Carriers</h1>
                    <form>
                        <div className="form mb-3">
                            <input value={filterValue} onChange={handleFilterVal} placeholder="Search by Name" name="filter-value" id="filter-value" className="form-control" />
                        </div>
                    </form>
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th onClick={() => handleSort("name")}>Name {renderSortArrow("name")}</th>
                                <th onClick={() => handleSort("address")}>Address {renderSortArrow("address")}</th>
                                <th onClick={() => handleSort("email")}>Email {renderSortArrow("email")}</th>
                                <th onClick={() => handleSort("phone_number")}>Phone Number {renderSortArrow("phone_number")}</th>
                                <th onClick={() => handleSort("dot")}>DOT # {renderSortArrow("dot")}</th>
                                <th onClick={() => handleSort("mc")}>MC # {renderSortArrow("mc")}</th>
                                <th>Attachments</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredCarriers().map((carrier) => {
                                return (
                                    <tr key={carrier.name}>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.name}
                                                    onChange={(e) => handleInputChange(e, "name")}
                                                    />
                                            ) : (
                                                carrier.name
                                            )}
                                        </td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.address}
                                                    onChange={(e) => handleInputChange(e, "address")}
                                                    />
                                            ) : (
                                                carrier.address
                                            )}
                                        </td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.email}
                                                    onChange={(e) => handleInputChange(e, "email")}
                                                    />
                                            ) : (
                                                carrier.email
                                            )}
                                        </td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.phone_number}
                                                    onChange={(e) => handleInputChange(e, "phone_number")}
                                                    />
                                            ) : (
                                                carrier.phone_number
                                            )}
                                        </td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.dot}
                                                    onChange={(e) => handleInputChange(e, "dot")}
                                                    />
                                            ) : (
                                                carrier.dot
                                            )}
                                        </td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <input
                                                    type="text"
                                                    value={editCarrier.mc}
                                                    onChange={(e) => handleInputChange(e, "mc")}
                                                    />
                                            ) : (
                                                carrier.mc
                                            )}
                                        </td>
                                        <td></td>
                                        <td>
                                            {editCarrier && editCarrier.dot === carrier.dot ? (
                                                <>
                                                    <i
                                                        className="fas fa-check"
                                                        style={{ cursor: "pointer", marginRight: 10 }}
                                                        onClick={() => handleUpdate(carrier.dot)}
                                                        ></i>
                                                    <i
                                                        className="fas fa-ban"
                                                        style={{ cursor: "pointer" }}
                                                        onClick={() => setEditCarrier(null)}
                                                    ></i>
                                                </>
                                            ) : (
                                                <>
                                                    <i
                                                        className="fas fa-trash"
                                                        style={{ cursor: "pointer", marginRight: 10 }}
                                                        onClick={() => handleDelete(carrier.dot)}
                                                    ></i>
                                                    <i
                                                        className="fas fa-pen-to-square"
                                                        style={{ cursor: "pointer" }}
                                                        onClick={() => handleEdit(carrier)}
                                                    ></i>
                                                </>
                                            )}
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                        {deleteSuccess && (
                            <div className="alert alert-danger mt-3 bg-danger text-white" role="alert">
                                Carrier successfully deleted.
                            </div>
                        )}
                    </table>
                </div>
            </div>
        </>
    );
}

export default CarrierList;
