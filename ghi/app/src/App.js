import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import CarrierForm from "./CarrierForm";
import CarrierList from "./CarrierList";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import SignUpForm from "./SignUpForm";
import LoginForm from "./LoginForm";
import MainPage from "./Main";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container-fluid" style={{paddingRight: 0, paddingLeft: 0, }}>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="login" element={<LoginForm />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="new-automobile" element={<AutomobileForm />} />
          <Route path="carrier/new" element={<CarrierForm />} />
          <Route path="carriers" element={<CarrierList />} />
          <Route path="signup" element={<SignUpForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
