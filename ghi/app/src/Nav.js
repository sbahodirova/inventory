import { NavLink, useNavigate } from 'react-router-dom';
import './css/styles.css'
import { useEffect, useState } from 'react';

function Nav() {

  const navigate = useNavigate();
  const [isLoggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    const authToken = localStorage.getItem('token');
    setLoggedIn(!!authToken);
    console.log('Is logged in:', !!authToken);
  }, []);


  const handleLogout = () => {
    localStorage.removeItem('token');
    setLoggedIn(false);
    navigate('/');
  };

  useEffect(() => {
    const handleSidebarToggle = () => {
      document.body.classList.toggle('sb-sidenav-toggled');
      localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
    };

    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
      sidebarToggle.addEventListener('click', handleSidebarToggle);
    }

    return () => {
      if (sidebarToggle) {
        sidebarToggle.removeEventListener('click', handleSidebarToggle);
      }
    };
  }, []);



  return (
    <>
      <div className="sb-nav-fixed fixed-top">
        <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
          <NavLink className="navbar-brand ps-3" to="/">CarCar</NavLink>
          <button className="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!">
            <i className="fas fa-bars"></i>
          </button>
          <ul className="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                id="navbarDropdown"
                to="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <i className="fas fa-user fa-fw"></i>
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                {isLoggedIn ? (
                  <>
                    <li>
                      <NavLink className="custom-dropdown-item" to="/" onClick={handleLogout}>
                        Logout
                      </NavLink>
                    </li>
                  </>
                ) : (
                  <>
                    <li>
                      <NavLink className="custom-dropdown-item" to="/signup">
                        SignUp
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="custom-dropdown-item" to="/login">
                        Login
                      </NavLink>
                    </li>
                  </>
                )}
              </ul>
            </li>
          </ul>
        </nav>
        <div id="layoutSidenav">
          <div id="layoutSidenav_nav">
            <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
              <div className="sb-sidenav-menu">
                <div className="nav">
                  <div className="sb-sidenav-menu-heading">Core</div>
                  <NavLink className="nav-link" to="/">
                    <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                    Dashboard
                  </NavLink>
                  <div className="sb-sidenav-menu-heading">Interface</div>
                  <NavLink className="nav-link collapsed" to="/automobiles" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                    Automobiles
                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                  </NavLink>
                  <div className="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav className="sb-sidenav-menu-nested nav">
                      <NavLink className="nav-link" to="automobiles">Automobile List</NavLink>
                      <NavLink className="nav-link" to="/new-automobile">New Automobile</NavLink>
                    </nav>
                  </div>
                  <NavLink className="nav-link collapsed" to="/carriers" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                    Carriers
                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                  </NavLink>
                  <div className="collapse" id="collapsePages" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav className="sb-sidenav-menu-nested nav">
                      <NavLink className="nav-link" to="/carriers">Carrier List</NavLink>
                      <NavLink className="nav-link" to="/carrier/new">New Carrier</NavLink>
                    </nav>
                  </div>
                  {/* <div className="sb-sidenav-menu-heading">Addons</div>
                            <NavLink className="nav-link" to="/charts">
                                <div className="sb-nav-link-icon"><i className="fas fa-chart-area"></i></div>
                                Charts
                            </NavLink>
                            <NavLink className="nav-link" to="/tables">
                                <div className="sb-nav-link-icon"><i className="fas fa-table"></i></div>
                                Tables
                            </NavLink> */}
                </div>
              </div>
              <div className="sb-sidenav-footer">
                <div className="small">Logged in as:</div>
                Start Bootstrap
              </div>
            </nav>
          </div>
        </div>
      </div>
    </>
  )
}


export default Nav;
