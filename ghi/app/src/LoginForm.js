import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
function LoginForm() {
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    });
    const [errors, setErrors] = useState({});
    const navigate = useNavigate();


    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const formDataEncoded = new URLSearchParams(formData).toString();
        const response = await axios.post("http://localhost:8100/api/login/", formDataEncoded)
            if (response.data.success) {
                localStorage.setItem('token', response.data.token)
                navigate("/automobiles");
            } else {
                setErrors(response.data.errors);
            }
        } catch(error) {
            console.error('Error:', error);
    };
}

    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="bg-light p-4 text-dark">
                        <h1>Login</h1>
                        <form onSubmit={handleSubmit} id="login-form">
                            <div className="form-floating mb-3">
                                <input
                                    type="text"
                                    name="email"
                                    value={formData.email}
                                    onChange={handleChange}
                                    placeholder="Email"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="email">Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    type="password"
                                    name="password"
                                    value={formData.password}
                                    onChange={handleChange}
                                    placeholder="Password"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                            {errors.non_field_errors && (
                                <div className="alert alert-danger" role="alert">
                                    {errors.non_field_errors}
                                </div>
                            )}
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoginForm;
