import React, { useState, useEffect } from "react";
import './css/styles.css'

function AutomobileForm() {
    const [vin, setVin] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [year, setYear] = useState("");
    const [carrier, setCarrier] = useState("");
    const [pickup, setPickup] = useState("")
    const [delivery, setDelivery] = useState("")
    const [dateDelivery, setDateDelivery] = useState("")
    const [successClass, setSuccessClass] = useState(
        "alert alert-success d-none"
    );

    const [carriers, setCarriers] = useState([]);
    const fetchCarriers = async () => {
        const response = await fetch("http://localhost:8100/api/carriers/");
        if (response.ok) {
            const data = await response.json();
            setCarriers(data.carriers);
        }
    };

    useEffect(() => {
        fetchCarriers();
    }, []);

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };
    const handleModelChange = (event) => {
        setModel(event.target.value);
    };
    const handleYearChange = (event) => {
        setYear(event.target.value);
    };
    const handleCarrierChange = (event) => {
        setCarrier(event.target.value);
    };
    const handlePickupChange = (event) => {
        setPickup(event.target.value);
    };
    const handleDeliveryChange = (event) => {
        setDelivery(event.target.value);
    };
    const handleDateDeliveryChange = (event) => {
        setDateDelivery(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model = model;
        data.year = year;
        data.vin = vin;
        data.location_of_pickup = pickup;
        data.location_of_delivery = delivery;
        data.date_of_delivery = dateDelivery;
        data.carrier = carrier

        const url = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setSuccessClass("alert alert-success");
            setVin("");
            setManufacturer("");
            setModel("");
            setYear("");
            setCarrier("");
            setPickup("");
            setDelivery("");
            setDateDelivery("");
        }
    };

    return (
        <>
            <div style={{ height: '100vh', overflowY: 'auto' }}>
            <div className="container-fluid">
                <div className="row justify-content-center mt-5">
                    <div className="col-md-6">
                        <div className="bg-light p-4 text-dark">
                            <h1>Add a new automobile to your inventory</h1>
                            <form onSubmit={handleSubmit} id="create-automobile-form">
                                <div className="form-floating mb-3">
                                    <input value={vin} onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                                    <label htmlFor="vin">VIN</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={model} onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                                    <label htmlFor="model">Model</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={year} onChange={handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                                    <label htmlFor="year">Year of manufacture</label>
                                </div>
                                <div className="mb-3">
                                    <select value={carrier} onChange={handleCarrierChange} required name="carrier" id="carrier" className="form-select">
                                        <option value="">Choose a carrier</option>
                                        {carriers.map((carrier) => {
                                            return (
                                                <option key={ carrier.dot } value={ carrier.dot }>
                                                { carrier.name }
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={pickup} onChange={handlePickupChange} placeholder="Pickup Location" required type="text" name="location_of_pickup" id="location_of_pickup" className="form-control"/>
                                    <label htmlFor="pickup">Pickup Location</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={delivery} onChange={handleDeliveryChange} placeholder="Delivery Location" required type="text" name="location_of_delivery" id="location_of_delivery" className="form-control"/>
                                    <label htmlFor="delivery">Delivery Location</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input
                                        value={dateDelivery}
                                        onChange={handleDateDeliveryChange}
                                        placeholder="Date of Delivery"
                                        type="datetime-local"
                                        name="datetime-local"
                                        id="datetime-local"
                                        className="form-control"
                                    />
                                    <label htmlFor="datetime-local">Date of Delivery</label>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                            <div className={successClass} role="alert">
                                {" "}
                                Automobile field has been created.{" "}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AutomobileForm;
