function MainPage() {
    return (
        <div className="container mt-5">
            <h1>Welcome to Your Inventory</h1>
            <p>This is the main page of your inventory management system.</p>
            {/* Add any additional content or functionality here */}
        </div>
    );
}

export default MainPage;
