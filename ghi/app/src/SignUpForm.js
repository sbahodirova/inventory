import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function SignupForm() {
    const [formData, setFormData] = useState({
        username: "",
        email: "",
        password: "",
        password_confirmation: "",
    });
    const [emailInUse, setEmailInUse] = useState(false);
    const [passwordsDoNotMatch, setPasswordsDoNotMatch] = useState(false);


    const navigate = useNavigate();

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value });
    };
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const formDataEncoded = new URLSearchParams(formData).toString();
            const response = await axios.post("http://localhost:8100/api/signup/", formDataEncoded);
            if (response.data.success) {
                localStorage.setItem('token', response.data.token)
                navigate("/automobiles");
            } else {
                if (response.data.errors.email) {
                    setEmailInUse(true);
                } else if (response.data.errors.password) {
                    setPasswordsDoNotMatch(true);
                } else {
                    setEmailInUse(false);
                    setPasswordsDoNotMatch(false);
                }
            }
        } catch (error) {
            console.error("Error:", error);
        }
    };


    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="bg-light p-4 text-dark">
                        <h1>Create Account</h1>
                        <form onSubmit={handleSubmit} id="signup-form">
                            <div className="form-floating mb-3">
                                <input
                                    type="text"
                                    name="username"
                                    value={formData.username}
                                    onChange={handleChange}
                                    placeholder="Username"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="username">Username</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    type="email"
                                    name="email"
                                    value={formData.email}
                                    onChange={handleChange}
                                    placeholder="Email"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="email">Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    type="password"
                                    name="password"
                                    value={formData.password}
                                    onChange={handleChange}
                                    placeholder="Password"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    type="password"
                                    name="password_confirmation"
                                    value={formData.password_confirmation}
                                    onChange={handleChange}
                                    placeholder="Confirm Password"
                                    required
                                    className="form-control"
                                />
                                <label htmlFor="password_confirmation">Confirm Password</label>
                            </div>
                            {emailInUse && (
                                <div className="alert alert-danger" role="alert">
                                    Email is already in use. Please choose a different one.
                                </div>
                            )}
                            {passwordsDoNotMatch && (
                                <div className="alert alert-danger" role="alert">
                                    Passwords do not match. Please check your input.
                                </div>
                            )}
                            <button type="submit" className="btn btn-primary">
                                Create Account
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SignupForm;
