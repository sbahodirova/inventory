import React, { useState } from "react";


function CustomerForm() {
  const [customerName, setCustomerName] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [dot, setDot] = useState("");
  const [mc, setMc] = useState("");
  // const [attachments, setAttachments] = useState([]);


  const handleCustomerNameChange = (e) => {
    setCustomerName(e.target.value);
  };
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handleAddressChange = (e) => {
    setAddress(e.target.value);
  };
  const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value);
  };
  const handleDotChange = (e) => {
    setDot(e.target.value);
  };
  const handleMcChange = (e) => {
    setMc(e.target.value);
  };

  // const handleAttachmentsChange = (e) => {
  //   const files = Array.from(e.target.files);
  //   setAttachments(files);
  // };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = customerName;
    data.email = email;
    data.address = address;
    data.phone_number = phoneNumber;
    data.dot = dot;
    data.mc = mc;
    // data.attachments = attachments;
    // attachments.forEach((file, index) => {
    //   data.append(`attachments[${index}]`, file);
    // });

    const url = `http://localhost:8100/api/carriers/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setCustomerName("");
      setEmail("");
      setAddress("");
      setPhoneNumber("");
      setDot("");
      setMc("");
      // setAttachments([]);
    }
  };

  return (
    <>
      <div style={{ height: '100vh', overflowY: 'auto' }}>
        <div className="container-fluid">
          <div className="row justify-content-center mt-5">
            <div className="col-md-6">
              <div className="bg-light p-4 text-dark">
              <h1>Add a new carrier</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input onChange={handleCustomerNameChange} value={customerName} placeholder="Name" required name="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmailChange} value={email} placeholder="Email" required name="email" className="form-control"/>
                  <label htmlFor="email">Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAddressChange} value={address} placeholder="Address" required name="address" type="text" id="address" className="form-control"/>
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="Phone Number" required name="phone_number" type="number" id="phone_number" className="form-control"/>
                  <label htmlFor="phone_number">Phone Number</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleDotChange} value={dot} placeholder="DOT Number" required name="dot" type="number" id="dot" className="form-control"/>
                  <label htmlFor="dot">DOT Number</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMcChange} value={mc} placeholder="MC Number" required name="mc" type="number" id="mc" className="form-control"/>
                  <label htmlFor="mc">MC Number</label>
                </div>
                {/* <div className="form-group mb-3">
                  <input
                    onChange={handleAttachmentsChange}
                    type="file"
                    accept=".pdf, .jpg, .png, .jpeg"
                    multiple
                    id="attachments"
                    className="form-control"
                  />
                </div> */}
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CustomerForm;
