from django.contrib import admin
from .models import Automobile, Carrier

admin.site.register(Automobile)
admin.site.register(Carrier)
