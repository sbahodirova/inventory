from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class Carrier(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    email = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=200)
    dot = models.IntegerField(unique=True, null=True)
    mc = models.IntegerField(unique=True, null=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    # attachments = models.ManyToManyField(Attachment, null=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_list_carriers", kwargs={"pk": self.pk})

class Automobile(models.Model):
    manufacturer = models.CharField(max_length=100, default="MANUFACTURER NOT SPECIFIED")
    model = models.CharField(max_length=100, default="MODEL NOT SPECIFIED")
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    status = models.CharField(max_length=200, default="AVAILABLE")
    date_of_delivery = models.DateTimeField(null=True, blank=True)
    location_of_pickup = models.CharField(max_length=200, default="NONE SPECIFIED")
    location_of_delivery = models.CharField(max_length=200, default="NONE SPECIFIED")
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )


    carrier = models.ForeignKey(
        Carrier,
        related_name="carriers",
        on_delete=models.CASCADE,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})
