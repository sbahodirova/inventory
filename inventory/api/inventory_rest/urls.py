from django.urls import path
from .views import (
    api_automobile,
    api_automobiles,
    api_carriers,
    api_carrier,
)

urlpatterns = [
    path(
        "automobiles/",
        api_automobiles,
        name="api_automobiles",
    ),
    path(
        "automobiles/<str:vin>/",
        api_automobile,
        name="api_automobile",
    ),
    path(
        "carriers/",
        api_carriers,
        name="api_carriers",
    ),
    path(
        "carrier/<str:dot>/",
        api_carrier,
        name="api_carrier",
    )
]
