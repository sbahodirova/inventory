from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
import json
from .encoders import (
    AutomobileEncoder,
    CarrierEncoder,
)
from .models import Automobile, Carrier
from datetime import datetime


@login_required
@require_http_methods(["GET", "POST"])
def api_automobiles(request):
    if request.method == "GET":
        autos = Automobile.objects.filter(user=request.user).order_by("vin")
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileEncoder,
        )
    else:
        content = json.loads(request.body)
        content['user'] = request.user
        carrier_id = content["carrier"]
        carrier = Carrier.objects.get(dot=carrier_id)
        content['carrier'] = carrier
        auto = Automobile.objects.create(**content)
        return JsonResponse(
            auto,
            encoder=AutomobileEncoder,
            safe=False,
        )

@login_required
@require_http_methods(["DELETE", "GET", "PUT"])
def api_automobile(request, vin):
    if request.method == "GET":
        try:
            auto = Automobile.objects.get(vin=vin, user=request.user)
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False
            )
        except Automobile.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            auto = Automobile.objects.get(vin=vin)
        except Automobile.DoesNotExist:
            return JsonResponse({"message": "Automobile does not exist"}, status=404)

        try:
            data = json.loads(request.body.decode('utf-8'))

            auto.manufacturer = data.get("manufacturer", auto.manufacturer)
            auto.model = data.get("model", auto.model)
            auto.year = data.get("year", auto.year)
            auto.vin = data.get("vin", auto.vin)

            date_of_delivery_str = data.get("date_of_delivery", auto.date_of_delivery)
            auto.date_of_delivery = datetime.strptime(date_of_delivery_str, '%Y-%m-%dT%H:%M:%S.%fZ')

            auto.location_of_pickup = data.get("location_of_pickup", auto.location_of_pickup)
            auto.location_of_delivery = data.get("location_of_delivery", auto.location_of_delivery)
            carrier_id = data.get("carrier")
            if carrier_id:
                try:
                    carrier = Carrier.objects.get(name=carrier_id)
                    auto.carrier = carrier
                except Carrier.DoesNotExist:
                    return JsonResponse({"message": "Carrier does not exist"}, status=400)

            auto.full_clean()
            auto.save()

            return JsonResponse(
                {"message": "Automobile updated successfully"},
                encoder=AutomobileEncoder,
                safe=False,
            )
        except json.JSONDecodeError as e:
            return JsonResponse({"message": f"Error decoding JSON: {e}"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)

    elif request.method == "DELETE":
        try:
            auto = Automobile.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False,
            )
        except Automobile.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        try:
            auto = Automobile.objects.get(vin=vin)
            props = ["color", "year"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False,
            )
        except Automobile.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@login_required
@require_http_methods(["GET", "POST"])
def api_carriers(request):
    if request.method == "GET":
        carriers = Carrier.objects.filter(user=request.user)
        return JsonResponse(
            {"carriers": list(carriers.values())},
            encoder=CarrierEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        content['user'] = request.user
        carrier = Carrier.objects.create(**content)
        return JsonResponse(
            carrier,
            encoder=CarrierEncoder,
            safe=False,
        )


@login_required
@require_http_methods(["DELETE", "GET", "PUT"])
def api_carrier(request, dot):
    if request.method == "GET":
        try:
            carriers = Carrier.objects.filter(user=request.user)
            return JsonResponse(
                {"carriers": carriers},
                encoder=CarrierEncoder,
                safe=False,
            )
        except Carrier.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            carrier = Carrier.objects.get(dot=dot, user=request.user)
        except Carrier.DoesNotExist:
            return JsonResponse({"message": "Carrier does not exist"}, status=404)

        try:
            data = json.loads(request.body.decode('utf-8'))

            carrier.name = data.get("name", carrier.name)
            carrier.address = data.get("address", carrier.address)
            carrier.email = data.get("email", carrier.email)
            carrier.phone_number = data.get("phone_number", carrier.phone_number)
            carrier.dot = data.get("dot", carrier.dot)
            carrier.mc = data.get("mc", carrier.mc)

            carrier.full_clean()
            carrier.save()

            return JsonResponse(
                {"message": "Carrier updated successfully"},
                encoder=CarrierEncoder,
                safe=False,
            )
        except json.JSONDecodeError as e:
            return JsonResponse({"message": f"Error decoding JSON: {e}"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    elif request.method == "DELETE":
        try:
            carrier = Carrier.objects.get(dot=dot, user=request.user)
            print(carrier)
            carrier.delete()
            return JsonResponse(
                carrier,
                encoder=CarrierEncoder,
                safe=False,
            )
        except Carrier.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
