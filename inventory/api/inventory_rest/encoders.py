from common.json import ModelEncoder
from .models import Automobile, Carrier


class CarrierEncoder(ModelEncoder):
    model = Carrier
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
        "email",
        "dot",
        "mc",
    ]

class AutomobileEncoder(ModelEncoder):
    model = Automobile
    properties = [
        "id",
        "status",
        "manufacturer",
        "year",
        "vin",
        "model",
        "date_of_delivery",
        "location_of_pickup",
        "location_of_delivery",
        "carrier",
    ]
    def get_extra_data(self, o):
        return {"carrier": o.carrier.name}
