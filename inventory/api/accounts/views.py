from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import JsonResponse
import jwt


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            if User.objects.filter(email=email).exists():
                return JsonResponse({'error': 'Email already in use'}, status=400)

            if password != password_confirmation:
                return JsonResponse({'error': 'Passwords do not match'}, status=400)

            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    email=email,
                    password=password
                )
                login(request, user)
                token = jwt.encode({'username': username}, 'secret_key', algorithm='HS256')
                return JsonResponse({'success': True, 'message': 'User registered successfully', 'token': token})
            else:
                return JsonResponse({"success": False, "errors": {"password": "Passwords do not match"}})
    else:
        form = SignUpForm()
    return JsonResponse({"success": False, "errors": form.errors})


def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = User.objects.filter(email=email).first()

            user = authenticate(
                request,
                username=user.username,
                password=password,
                )
            if user is not None:
                login(request, user)
                token = jwt.encode({'email': email}, 'secret_key', algorithm='HS256')
                return JsonResponse({"success": True, 'message': 'User logged in successfully', 'token': token })
            else:
                return JsonResponse({"success": False, "errors": {"email": "User does not exist"}})
    else:
        form = LogInForm()
    return JsonResponse({"success": False, "errors": form.errors})

def user_logout(request):
    logout(request)
    return JsonResponse({"success": True})
